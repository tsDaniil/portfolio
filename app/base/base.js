/**
 * @class Base
 * @extends Funcs
 */
var Base = Funcs.extend({

    /**
     * @private
     */
    _events: null,

    /**
     * @method trigger
     * @param {String} eventName
     * @param {*|Array} [arg]
     * @param {Boolean} [async]
     * @returns @this
     */
    trigger: function (eventName, arg, async) {

        if (!this._events || !this._events[eventName]) {
            return this;
        }

        if (!(arg instanceof Array)) {
            arg = [arg];
        }

        var iteration = function () {

            var arrEvents = this._events[eventName].slice();

            arrEvents.forEach(function (handler) {
                handler.apply(window, arg);
            });

        }.bind(this);

        if (async) {
            setTimeout(function () {
                iteration();
            }.bind(this), 0);
        } else {
            iteration();
        }

        return this;
    },

    /**
     * @method bind
     * @param {String|Object} eventName
     * @param {function|Array} [handler]
     * @returns @this
     */
    bind: function (eventName, handler) {

        if (eventName == undefined) {
            console.log("Неверные параметры!");
            return this;
        }

        if (eventName instanceof Object) {
            for (var key in eventName) {
                if (eventName.hasOwnProperty(key))
                    this.bind(key, eventName[key]);
            }
            return this;
        }

        if (handler == undefined) {
            console.log("Неверные параметры!");
            return this;
        }

        if (!this._events) {
            this._events = {};
        }

        if (!this._events[eventName]) {
            this._events[eventName] = [];
        }

        if (!(handler instanceof Array)) {
            this._events[eventName].push(handler);
        } else {
            this._events[eventName] = this._events[eventName].concat(handler);
        }

        return this;
    },

    /**
     * @method unbind
     * @param {String|Object} [eventName]
     * @param {function} [handler]
     * @returns @this
     */
    unbind: function (eventName, handler) {

        if (eventName == undefined) {
            this._events = {};
            return this;
        } else if (eventName instanceof Object) {
            for (var key in eventName)
                if (eventName.hasOwnProperty(key))
                    this.unbind(key, key[eventName]);

            return this;
        }

        if (handler == undefined) {
            if (this._events && eventName in this._events) {
                this._events[eventName] = [];
                return this;
            }
            return this;
        }

        for (var i = 0; i < this._events[eventName].length; i++) {

            if (this._events[eventName][i] == handler) {
                this._events[eventName].splice(i, 1);
                break;
            }

        }

        return this;
    }

});