/**
 * @class Funcs
 */
var Funcs = new Class({

    /**
     * Провкряем массив ли это
     * @param param
     * @returns {boolean}
     */
    isArray: function (param) {
        return param instanceof Array;
    },

    /**
     * Проверяем объект ли это
     * @param param
     * @returns {boolean}
     */
    isObject: function (param) {
        return (typeof param == "object" && !this.isArray(param));
    },

    /**
     * Проверяем принадлежность к чилам
     * @param param
     * @returns {boolean}
     */
    isNumber: function (param) {
        return typeof param == "number";
    },

    /**
     * Проверяем строка ли это
     * @param param
     * @returns {boolean}
     */
    isString: function (param) {
        return typeof param == "string";
    },

    /**
     * Проверяем функция ли это
     * @param param
     * @returns {boolean}
     */
    isFunction: function (param) {
        return typeof param == "function";
    },

    /**
     * Проверяем принадлежность объекта к типу
     * @param object
     * @param type
     * @returns {boolean}
     */
    isObjectType: function (object, type) {
        return object instanceof type;
    },

    /**
     * Проверяет принадлежность объекта к типу jQuery
     * @param object
     * @returns {Boolean}
     */
    isJquery: function (object) {
        return this.isObjectType(object, $);
    },

    /**
     * Перебор объекта
     * @param {Object} obj итерируемый объект
     * @param {function(value, key)} func функция выполняемая для каждого элемента
     */
    objItar: function (obj, func) {
        for (var key in obj)
            if (obj.hasOwnProperty(key))
                func.call(window, obj[key], key);
    },

    /**
     * Клонируем объект (рекурсивно);
     * @param obj
     * @returns {Object}
     */
    cloneObj: function (obj) {

        var result = {};

        function clone(obj, newObj) {

            Funcs.objItar(obj, function (child, name) {
                if (this.isArray(child)) {
                    newObj[name] = child.slice();
                } else if (this.isObject(child)) {
                    newObj[name] = {};
                    clone(child, newObj[name]);
                } else {
                    newObj[name] = child;
                }
                return newObj;
            });

            return newObj;
        }

        return clone(obj, result);
    },

    /**
     * Приводит числа к формату "02" от "2"
     * @param {Number} num
     * @returns {string}
     */
    len: function (num) {
        return ((num + "").length < 2) ? "0" + num : num;
    },

    validateDate: function (date) {
        if (date && !(date instanceof Date)) {
            date = new Date(date);
            if (date == "Invalid Date")
                date = null;
        }
        if (!date) {
            date = new Date();
        }
        return date;
    },

    /**
     * Получаем время формата hh:mm
     * @param {String|Number|Date} [date]
     * @returns {string}
     */
    getTime: function (date) {
        date = this.validateDate(date);
        return Main.len(date.getHours()) + ":" + Main.len(date.getMinutes());
    },

    /**
     * Получаем время формата hh:mm:ss
     * @param {String|Number|Date} [date]
     * @returns {string}
     */
    getFullTime: function (date) {
        date = this.validateDate(date);
        return Main.getTime()+ ":" + Main.len(date.getSeconds());
    },

    /**
     * Получаем дату формата dd.mm.yyyy
     * @param {String|Number|Date} [date]
     * @returns {string}
     */
    getDate: function (date) {
        date = this.validateDate(date);
        return Main.len(date.getDay()) + "." + Main.len(date.getMonth() + 1) + "." + date.getFullYear();
    },

    convertToDate: function (date) {

        if (date.indexOf(":") -1 != 0) {

            date = date.split(":");
            if (date.length == 2) {
                return new Date(0, 0, 0, date[0], date[1], date[2]);
            } else {
                return new Date(0, 0, 0, date[0], date[1]);
            }

        } else {

            date = date.split(".");

            date = date.map(function (dat, i) {
                if (i == 1) {
                    return (+dat) - 1
                } else {
                    return +dat
                }
            });

            return new Date(date[2], date[1], date[0]);

        }
    }
});