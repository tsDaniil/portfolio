/**
 * @class Main
 * @extends Base
 */
var Main = Base.extend({

    /**
     * @constructor
     */
    constructor: function () {

        this.components = {

            Bootstrap: {version: "3.1.1", state: "loaded"},
            Angular: {version: "1.3.0-beta.1", state: "loaded"},
            jQuery: {version: "2.1.0", state: "loaded"}

        };

        this.menuState = "unloaded";

        this.bind("menu:loaded", function () {
            this.menuState = "loaded";
        }.bind(this));

    },

    onMenuLoad: function (funck) {

        if (this.menuState == "unloaded") {

            this.bind("menu:loaded", funck);

        } else {

            funck();

        }

    }
});

Main = new Main;