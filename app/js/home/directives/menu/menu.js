angular.module('home')
    .directive('myMenu', [

        "StateManager",

        function (StateManager) {

            return {
                restrict: 'E',
                templateUrl: "app/js/home/directives/menu/menu.tpl.html",
                link: {
                    pre: function ($scope, $element, $attrs) {

                        /**
                         * Клас управляющий менюшкой
                         * @class MainControl
                         */
                        var MainControl = new Class({
                            /**
                             * @constructor
                             * @param {jQuery} menu менюшки
                             */
                            constructor: function (menu) {

                                /**
                                 * @type {jQuery}
                                 */
                                this.node = menu;

                                this.init();

                            },

                            /**
                             * Метод инициализации
                             */
                            init: function () {

                                StateManager.setNode(this.node).setSize(this.node.first().width() + 2);

                                this.setHandlers().show();

                            },

                            /**
                             * Назначаем обработчики
                             * @returns {MainControl}
                             */
                            setHandlers: function () {

                                $(window).bind("resize", function () {
                                    StateManager["set" + this.state]();
                                }.bind(this));

                                Main.bind("setState", function (state) {
                                    this.state = state;
                                    StateManager["set" + state]();
                                }.bind(this));

                                return this;
                            },


                            /**
                             * Показываем меню (однократно, меню приезжает сверху)
                             * @returns {MainControl}
                             */
                            show: function () {

                                this.node.animate({
                                    top: "50%"
                                }, 700);

                                setTimeout(function () {
                                    Main.trigger("menu:loaded");
                                }.bind(this), 700);

                                return this;
                            }
                        });

                        $scope.mainControl = new MainControl($element.find(".menu"));

                    }
                }
            }
        }]
    );