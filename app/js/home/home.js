angular.module("home", [])
    .config([
        '$stateProvider',
        '$urlRouterProvider',
        function ($stateProvider, $urlRouterProvider) {

            $urlRouterProvider

                .otherwise('/menu');

            $stateProvider

                .state('menu.skill', {
                    url: '/skill',
                    views: {
                        "tmp": {
                            template: ""
                        }
                    },
                    onEnter: function () {
                        Main.onMenuLoad(function () {
                            Main.trigger("setState", ["Skill"]);
                        });
                    }

                })

                .state('menu.skill.js', {
                    url: '/js',
                    views: {
                        "tmp": {
                            template: ""
                        }
                    },
                    onEnter: function () {
                        Main.onMenuLoad(function () {
                            Main.trigger("setState", ["Js"]);
                        });
                    }
                })

                .state('menu.skill.css', {
                    url: '/css',
                    views: {
                        "tmp": {
                            template: ""
                        }
                    },
                    onEnter: function () {
                        Main.onMenuLoad(function () {
                            Main.trigger("setState", ["Css"]);
                        });
                    }
                })

                .state('menu.skill.other', {
                    url: '/other',
                    views: {
                        "tmp": {
                            template: ""
                        }
                    },
                    onEnter: function () {
                        Main.onMenuLoad(function () {
                            Main.trigger("setState", ["SkillOther"]);
                        });
                    }
                })

                .state('menu.projects', {
                    url: '/projects',
                    views: {
                        "tmp": {
                            template: ""
                        }
                    },
                    onEnter: function () {
                        Main.onMenuLoad(function () {
                            Main.trigger("setState", ["Projects"]);
                        });
                    }
                })

                .state('menu.about', {
                    url: '/about',
                    views: {
                        "tmp": {
                            template: ""
                        }
                    },
                    onEnter: function () {
                        Main.onMenuLoad(function () {
                            Main.trigger("setState", ["About"]);
                        });
                    }
                })

                .state('menu.page-layout', {
                    url: '/page-layout',
                    views: {
                        "tmp": {
                            template: ""
                        }
                    },
                    onEnter: function () {
                        Main.onMenuLoad(function () {
                            Main.trigger("setState", ["PageLayout"]);
                        });
                    }
                })

                .state('menu.js-development', {
                    url: '/js-development',
                    views: {
                        "tmp": {
                            template: ""
                        }
                    },
                    onEnter: function () {
                        Main.onMenuLoad(function () {
                            Main.trigger("setState", ["JsDevelopment"]);
                        });
                    }
                })

        }]
    );