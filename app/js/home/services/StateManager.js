angular.module('home')
    .factory('StateManager', [

        "$uiModal",
        "$state",
        "LoadService",

        /**
         * @param $uiModal модальное окно от angular ui
         * @param $state
         * @param {LoadService} LoadService
         * @returns {StateManager}
         */
         function ($uiModal, $state, LoadService) {

            /**
             * @class StateManager
             * @extends Base
             */
            var StateManager = Base.extend({
                /**
                 * @constructor
                 */
                constructor: function () {
                    /**
                     * Background container
                     * @type {jQuery}
                     */
                    this.background = $(".background");
                    /**
                     * @type {number}
                     */
                    this.windowHeight = $(window).height() / 2;
                    /**
                     * @type {number}
                     */
                    this.windowWidth = $(window).width() / 2;
                    /**
                     * Site container
                     * @type {jQuery}
                     */
                    this.site = $("#site");
                    /**
                     * @type {number}
                     */
                    this.animationSpeed = 500;

                    this.setHandlers();

                },
                /**
                 * Binding handlers
                 * @method setHandlers
                 * @returns {StateManager}
                 */
                setHandlers: function () {

                    $(window).bind("resize", function () {

                        this.windowHeight = $(window).height() / 2;
                        this.windowWidth = $(window).width() / 2;

                    }.bind(this));

                    return this;
                },
                /**
                 * Set general node
                 * @method setNode
                 * @param {jQuery} node
                 * @returns {StateManager}
                 */
                setNode: function (node) {
                    /**
                     * @type {jQuery}
                     */
                    this.node = node;
                    return this;
                },
                /**
                 * Set size general node + border
                 * @method setSize
                 * @param {Number} size
                 * @returns {StateManager}
                 */
                setSize: function (size) {
                    this.size = size;
                    return this;
                },
                /**
                 * Animate
                 * @method animate
                 * @param {jQuery} element
                 * @param {Number} [left]
                 * @param {Number} [top]
                 * @returns {StateManager}
                 */
                animate: function (element, left, top) {
                    left = left || this.windowWidth;
                    top = top || this.windowHeight;

                    element.animate({
                        left: left,
                        top: top
                    }, this.animationSpeed);

                    return this;
                },
                /**
                 * @method setDefaultPosition
                 * @param {String} list
                 * @returns {StateManager}
                 */
                setDefaultPosition: function (list) {

                    if (typeof list === "undefined")
                        list = ".general, .skill, .about, .projects";

                    this.animate(this.node.not(list).stop());

                    setTimeout(function () {
                        this.node.not(list).hide();
                    }.bind(this), this.animationSpeed);

                    return this;
                },
                /**
                 * @method setStyleMap
                 * @param {String} className
                 * @returns {StateManager}
                 */
                setStyleMap: function (className) {

                    var tmp;

                    this.site.removeClass().addClass(className);

                    if (this.background.filter(".active").length == 0) {
                        this.background.first().addClass("active " + className).fadeIn(this.animationSpeed);
                        this.background.last().fadeOut(this.animationSpeed);
                    } else {

                        tmp = this.background.not(".active");
                        tmp.get(0).className = "background fadeOut active " + className;
                        tmp.fadeIn(this.animationSpeed);

                        this.background.not(tmp).fadeOut(this.animationSpeed).removeClass("active");
                        tmp = null;

                    }

                    return this;
                },
                /**
                 * @method setGeneral
                 * @returns {StateManager}
                 */
                setGeneral: function () {

                    this.setDefaultPosition();

                    this.setStyleMap("bg-General");

                    this.node.filter(".general, .skill, .about, .projects").show();

                    this.animate(this.node.filter(".general").stop());
                    this.animate(this.node.filter(".skill").stop(), this.windowWidth + this.size);
                    this.animate(this.node.filter(".about").stop(), undefined, this.windowHeight + this.size);
                    this.animate(this.node.filter(".projects").stop(), this.windowWidth - this.size);

                    return this;
                },
                /**
                 * @method setSkill
                 * @returns {StateManager}
                 */
                setSkill: function () {

                    this.setDefaultPosition(".general, .js, .css, .skill, .skill-other");

                    this.setStyleMap("bg-Skill");

                    this.node.filter(".general, .js, .css, .skill, .skill-other").show();

                    this.animate(this.node.filter(".general").stop(), this.windowWidth - this.size);
                    this.animate(this.node.filter(".js").stop(), undefined, this.windowHeight - this.size);
                    this.animate(this.node.filter(".css").stop(), this.windowWidth + this.size);
                    this.animate(this.node.filter(".skill").stop());
                    this.animate(this.node.filter(".skill-other").stop(), undefined, this.windowHeight + this.size);

                    return this;
                },
                /**
                 * @method setProjects
                 * @returns {StateManager}
                 */
                setProjects: function () {

                    this.setDefaultPosition(".projects, .general, .page-layout, .js-development");
                    this.node.filter(".page-layout, .js-development").show();

                    this.setStyleMap("bg-Projects");

                    this.animate(this.node.filter(".general").stop(), this.windowWidth + this.size);
                    this.animate(this.node.filter(".js-development").stop(), this.windowWidth - this.size);
                    this.animate(this.node.filter(".projects").stop());
                    this.animate(this.node.filter(".page-layout").stop(), undefined, this.windowHeight - this.size);

                    return this;
                },
                /**
                 * @method setJs
                 * @returns {StateManager}
                 */
                setJs: function () {
                    this.openModal('app/js/home/services/skill-js.tpl.html', 'menu.skill');
                    return this;
                },
                /**
                 * @method setCss
                 * @returns {StateManager}
                 */
                setCss: function () {
                    this.openModal('app/js/home/services/skill-css.tpl.html', 'menu.skill');
                    return this;
                },
                /**
                 * @method setSkillOther
                 * @returns {StateManager}
                 */
                setSkillOther: function () {
                    this.openModal('app/js/home/services/skill-other.tpl.html', 'menu.skill');
                    return this;
                },
                /**
                 * @method setAbout
                 * @returns {StateManager}
                 */
                setAbout: function () {
                    this.openModal('app/js/home/services/about.tpl.html', 'menu.home', 'lg');
                    return this;
                },
                /**
                 * @method setPageLayout
                 * @returns {StateManager}
                 */
                setPageLayout: function () {
                    this.openModal('app/js/home/services/pageLayout.tpl.html', 'menu.projects', 'lg');
                    return this;
                },
                /**
                 * @method setJsDevelopment
                 * @returns {StateManager}
                 */
                setJsDevelopment: function () {
                    this.openModal('app/js/home/services/js-development.tpl.html', 'menu.projects', 'lg');
                    return this;
                },
                /**
                 * @method openModal
                 * @param {String} url url to modal template
                 * @param {String} state state after close modal window
                 * @param {String} [size] size modal window
                 * @returns {StateManager}
                 */
                openModal: function (url, state, size) {

                    Main.trigger("load:start");

                    var modalInstance = $uiModal.open({
                        templateUrl: url,
                        size: size,
                        controller: function ($scope, $modalInstance) {
                            $scope.ok = function () {
                                $modalInstance.dismiss();
                            }
                        }
                    });

                    modalInstance.opened.then(function () {
                        Main.trigger("load:end");
                    });

                    modalInstance.result.then(function () {

                    }, function () {
                        $state.go(state);
                    });

                    return this;
                }

            });

            return new StateManager;
        }
    ]);