angular.module("app", ["vendor"])
    .config([
        '$stateProvider',
        '$urlRouterProvider',
        function ($stateProvider, $urlRouterProvider) {

            $urlRouterProvider

                .when("/menu", "/menu/home")
                .otherwise('/menu');

            $stateProvider

                .state("menu", {
                    url: "/menu",
                    views: {
                        "main": {
                            templateUrl: "app/js/home/menu.html"
                        }

                    }
                })

                .state('menu.home', {
                    url: '/home',
                    views: {
                        "tmp": {
                            template: ""
                        }
                    },
                    onEnter: function () {
                        Main.onMenuLoad(function () {
                            Main.trigger("setState", ["General"]);
                        });
                    }

                })

        }]
    );