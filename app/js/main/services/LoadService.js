angular.module('main')
    .factory('LoadService', [

        function () {

            /**
             * @class LoadService
             * @extends Base
             */
            var LoadService = Base.extend({

                /**
                 * @constructor
                 */
                constructor: function () {

                    this.setHandlers();

                },

                /**
                 * Назначаем обработчики
                 */
                setHandlers: function () {

                    Main.bind({
                        "load:start": function () {

                            this.load = $("<div class='load-container'><div class='load'><img src='app/img/ajax-loader.gif'><h5>Загрузка...</h5></div></div>");
                            $("body").append(this.load);
                            this.load.fadeIn();

                        }.bind(this),
                        "load:end": function () {

                            if (this.load)
                                this.load.fadeOut(200, function () {
                                    this.load.remove();
                                }.bind(this));


                        }.bind(this)
                    });

                }

            });

            return new LoadService;

        }
    ]
    );